#include <ArduinoJson-v6.15.1.h>

void runStringTest() {
  DynamicJsonDocument doc(512);
  String _ts1 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":13,\"cmd\":{\"code\":\"GY\"}}}");
  String _ts2 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":17,\"cmd\":{\"code\":\"PM\"}}}");
  String _ts3 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":19,\"cmd\":{\"code\":\"PM\",\"payload\":{\"power\":[true,false,false,false,true,false,false,false]}}}}");
  String _ts4 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":23,\"cmd\":{\"code\":\"BS\"}}}");
  String _ts5 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":4,\"cmd\":{\"code\":\"AP\"}}}");
  String _ts6 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":7,\"cmd\":{\"code\":\"SC\"}}}");
  String _ts7 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"esp\"},\"dest\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"type\":\"req\",\"seq\":9,\"cmd\":{\"code\":\"SC\",\"payload\":{\"speeds\":[1100,1200,1300,1400]}}}}");
  String _ts8 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"req\",\"seq\":1,\"cmd\":{\"code\":\"..\"}}}");
  String _ts9 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":1,\"cmd\":{\"code\":\"..\",\"status\":\"ok\"}}}");
  String _ts10 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":1,\"cmd\":{\"code\":\"AP\",\"status\":\"ok\",\"payload\":{\"version\":{\"major\":1,\"minor\":0}}}}}");
  String _ts11 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":11,\"cmd\":{\"code\":\"SC\",\"status\":\"ok\",\"payload\":{\"speeds\":[1000,1888,1777,1000]}}}}");
  String _ts12 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":15,\"cmd\":{\"code\":\"GY\",\"status\":\"ok\",\"payload\":{\"ypr\":[0,124,550]}}}}");
  String _ts13 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":21,\"cmd\":{\"code\":\"PM\",\"status\":\"ok\",\"payload\":{\"power\":[false,true,true,true,true,true,true,true]}}}}");
  String _ts14 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":25,\"cmd\":{\"code\":\"BS\",\"status\":\"ok\",\"payload\":{\"status\":{\"voltage\":12000,\"current\":45000}}}}}");
  String _ts15 = F("{\"ps\":{\"src\":{\"group\":\"pan\",\"unit\":\"mmcu\"},\"dest\":{\"group\":\"pan\",\"unit\":\"esp\"},\"type\":\"res\",\"seq\":5,\"cmd\":{\"code\":\"AP\",\"status\":\"ok\",\"payload\":{\"version\":{\"major\":1,\"minor\":0}}}}}");

  String *_testString[] = {&_ts1, &_ts2, &_ts3, &_ts4, &_ts5, &_ts6, &_ts7, &_ts8, &_ts9, &_ts10, &_ts11, &_ts12, &_ts13, &_ts14, &_ts15};

  static uint8_t _testIndex = 0;

  String *currentString = _testString[_testIndex];

  Serial.printf("Running string test: %u\r\n", _testIndex);
  Serial.printf("%s\r\n", (*currentString).c_str());

  deserializeJson(doc, *currentString);

  _testIndex = (_testIndex + 1) % 15;
}

void setup() {
  Serial.begin(115200);
}

void loop() {
  runStringTest();
}